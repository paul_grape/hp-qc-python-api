from requests.auth import HTTPBasicAuth
import logging
import os
import requests
import json
import cookies
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class ALMException(Exception):
    pass


logger = logging.getLogger(__name__)


def logging_(filename='hpalm.log'):
    global logger

    file_path = os.path.join(os.getcwd(), filename)

    logging.basicConfig(datefmt='%H:%M:%S',
                         format="[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s",
                         filename=file_path,
                         level=logging.NOTSET)

    logger = logging.getLogger(__name__)

class hpalm(object):
    headers = None

    def __init__(self, **kwargs):
        self.base_url = kwargs.get('base_url', None)
        self.username = kwargs.get('username', None)
        self.password = kwargs.get('password', None)
        self.domain = kwargs.get('domain', None)
        self.project = kwargs.get('project', None)
        self.verify = kwargs.get('verify', False)
        self.alm_version = kwargs.get('alm_version', 11)
        log_msg = kwargs.get('log_msg', 1)

        if not (kwargs['base_url'] or kwargs['username'] or kwargs['password'] or kwargs['domain'] or kwargs['project']):
            raise ALMException("Please provide all mandatory params")

        if log_msg:
            logging_()

    def login(self):
        s = requests.Session()
        url = "https://login.software.microfocus.com/msg/actions/doLogin.action"
        payload = "username=vin.pavel13@gmail.com&password=EMCpractice2018"
        headers = {
            'Content-Type': "application/x-www-form-urlencoded",
            'Cache-Control': "no-cache",
            'Postman-Token': "9bf04cd7-b1c1-4dd7-b746-32d3c7159adc"
        }
        response = s.post(url, data=payload, headers=headers)
        print(s.cookies)
        d = requests.utils.dict_from_cookiejar(s.cookies)

        session_url = self.base_url + '/qcbin/rest/site-session'
        LWSSO = d.get('LWSSO_COOKIE_KEY')
        d1 = {'LWSSO_COOKIE_KEY': d.get('LWSSO_COOKIE_KEY')}
        resp = requests.post(session_url, cookies=requests.utils.cookiejar_from_dict(d1))
        print("Site-session resposne cookies %s" % resp.cookies)
        print("rest/site-session response code: %s  " % resp.status_code)

        QCSession = resp.cookies.get('QCSession')
        XSRF_TOKEN = resp.cookies.get('XSRF-TOKEN')

        print(QCSession)
        print(XSRF_TOKEN)

        print(session_url)

        '''return resp.status_code'''

    def logout(self):
        uri = self.base_url + '/qcbin/authentication-point/logout'
        resp = requests.get(uri, headers=self.headers)
        if resp.status_code == 200:
            logger.info("%s  %s logged out alm" % (resp.status_code, self.username))
        else:
            raise ALMException("%s  %s failed to logged out from HP ALM" % (resp.status_code, self.username))
        return resp.status_code

    def getentity(self, entitytype, entityid=None, query=None):

        """Get an entity

        :param entityType: type of entity to get:

            tests/test-sets/test-configs/test-set-folders/test-instances/runs/release-cycles/defects

        :type entityType: str

        :param entityId: id of entity to get. If None returns all instances of entity

        :type entityId: str | int

        :param query: query string to filter data. e.g: {name[Basic]}

        :type query: str

        :return: requested entity(s)

        :rtype: dict

        """
